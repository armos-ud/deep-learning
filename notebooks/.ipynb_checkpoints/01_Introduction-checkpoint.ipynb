{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Analysis of Dynamic Systems"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Schedule:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Getting started\n",
    "- Introduction\n",
    "- Mathematical bases\n",
    "- Bode diagrams\n",
    "- Modeling with linear elements\n",
    "- State variables\n",
    "- Block diagrams\n",
    "- Time response\n",
    "- Frequency response\n",
    "- Stability\n",
    "- Root Locus\n",
    "- Final project\n",
    "- Course evaluation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Course Notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Download the master file, unzip it to a place on the hard disk easy to find (I recommend the root of the hard disk, or a folder called c:/temp), and from inside the folder run:\n",
    "\n",
    "*ipython notebook*\n",
    "\n",
    "You'll be fine from there..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Control Systems"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A system can behave in many ways, output variables can have different goals, for example:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Get to the destination in the shortest time.\n",
    "- Get to the destination in the shortest time and with the minimum consumption."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import SVG\n",
    "SVG('img/intro_fig1.svg')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Almost every aspect of everyday life is affected by some kind of control system."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The basic elements of a control system are:\n",
    "- Control goals\n",
    "- System components\n",
    "- Results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Purpose of the control:** To control the outputs *c* in a predetermined way, by means of the inputs *u* and the control system."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Feedback:** Its function is to reduce the error between the reference input and the output. It also ensures stability."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Plant:** is the combination of process and actuator. A plant is often referred to with a transfer function (in the s-domain) which indicates the relation between an input signal and the output signal of a system without feedback, commonly determined by physical properties of the system."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Open-loop control systems"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import SVG\n",
    "SVG('img/intro_fig2.svg')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Example:** A washing machine. The wash cycle is determined by the user's criteria. There are no sensors that measure the degree of cleaning of clothes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Closed-loop control systems"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import SVG\n",
    "SVG('img/intro_fig3.svg')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Uses feedback from the output to the input."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import SVG\n",
    "SVG('img/intro_fig4.svg')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example, what happens in an elevator in both cases? And in the system of opening and closing of a door?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The feedback has the following goals:\n",
    "- Reduce error between reference input and output.\n",
    "- Ensure stability."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Many times a physical plant can have feedback when analyzed in detail. When the variables of a system exhibit a closed sequence of cause and effect relationships, the system has feedback (input and output are related). For example, for the system:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import SVG\n",
    "SVG('img/intro_fig5.svg')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It can be shown that its transfer function is:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\\frac{c}{r} = \\frac{G}{1+GH}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Effect of feedback on gain:** From the above equation it is observed that the gain of the system is affected by a factor of $1+GH$. $G$ and $H$ are functions of the frequency, so that the gain can increase in some frequencies and decrease in others."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Effect of feedback on stability:** Stability is a concept that indicates the ability of a system to follow a control input. A system is unstable when its output is out of control or increases without limits. Again let's look at the equation when $GH = -1$. The output of the system is infinite for any finite input. Therefore, it can be said that feedback can cause instability in a system. It should be noted that this is not the only condition of instability."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the plant has $GH = -1$, and it is inevitable, it is possible to add another feedback loop, obtaining a new transfer function. Therefore, the global system can be stable if $F$ is properly selected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import SVG\n",
    "SVG('img/intro_fig6.svg')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\\frac{c}{r} = \\frac{\\frac{G}{1+GH}}{1+\\frac{GF}{1+GH}} = \\frac{\\frac{G}{1+GH}}{\\frac{1+GH+GF}{1+GH}}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\\frac{c}{r} = \\frac{G}{1+GH+GF}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general feedback has effects on performance, bandwidth, impedance, time response and frequency response."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Types of control systems"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- According to the method of analysis and design\n",
    " - Linear: A system based on the use of a linear operator.\n",
    " - Nonlinear\n",
    " - Time-invariant (TIV): System whose output does not depend explicitly upon time.\n",
    " - Time-variant (TV): Its outputs depend explicitly upon time.\n",
    " - If a time-invariant system is also linear, it is a LTI system (linear time-invariant).\n",
    "- According to the types of signals:\n",
    " - Continuous systems\n",
    " - Discrete systems\n",
    "- According to the components of the system\n",
    " - Electromechanical\n",
    " - Hydraulic\n",
    " - Biological\n",
    " - ...\n",
    "- According to its main purpose\n",
    " - Position system\n",
    " - Speed system\n",
    " - ..."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
