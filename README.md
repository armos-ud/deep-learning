# Deep learning

![Logo](https://gitlab.com/armos-ud/deep-learning/-/blob/master/logo.png "ARMOS")

## Course on Neural Networks prepared by the ARMOS research group.

**Deep learning** (deep structured learning or differential programming) is part of a family of machine learning methods based on artificial neural networks. The training material and code is developed in Python and Jupyter Notebook.
